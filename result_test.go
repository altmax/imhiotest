package main

import (
	"testing"

	"github.com/franela/goblin"
)

func TestGetConfigByQuery(t *testing.T) {
	testR := map[string]map[string]Config{
		"Develop.mr_robot": map[string]Config{
			"Database.processing": Config{
				DataID:   1,
				Host:     "localhost",
				Port:     "5432",
				Database: "devdb",
				User:     "mr_robot",
				Password: "secret",
				Schema:   "public",
			},
		},
		"Test.vpn": map[string]Config{
			"Rabbit.Log": Config{
				DataID:      2,
				Host:        "10.0.5.42",
				Port:        "5671",
				Virtualhost: "/",
				User:        "guest",
				Password:    "guest",
			},
		},
	}

	g := goblin.Goblin(t)
	g.Describe("#GetConfigByQuery", func() {
		g.It("Should return Develop.mr_robot config", func() {
			q := Query{
				Type: "Develop.mr_robot",
				Data: "Database.processing",
			}
			res, err := GetConfigByQuery(&q)
			g.Assert(err == nil).IsTrue()
			g.Assert(*res).Equal(testR["Develop.mr_robot"]["Database.processing"])
		})

		g.It("Should return Test.vpn config", func() {
			q := Query{
				Type: "Test.vpn",
				Data: "Rabbit.Log",
			}

			res, err := GetConfigByQuery(&q)
			g.Assert(err == nil).IsTrue()
			g.Assert(*res).Equal(testR[q.Type][q.Data])
		})

		g.It("Should return non nil error for not exists json-object", func() {
			q := Query{
				Type: "Test.vpn",
				Data: "Rabbit.Logging",
			}

			_, err := GetConfigByQuery(&q)
			g.Assert(err == nil).IsFalse()
		})

		g.It("Should return non nil error for not exists json-object", func() {
			q := Query{
				Type: "Develop.vpn",
				Data: "Rabbit.Logging",
			}

			_, err := GetConfigByQuery(&q)
			g.Assert(err == nil).IsFalse()
		})

		g.It("Shouldn return non nil error for empty query", func() {
			q := Query{}
			_, err := GetConfigByQuery(&q)
			g.Assert(err == nil).IsFalse()
		})

		g.It("Should return non nil error for nil query", func() {
			_, err := GetConfigByQuery(nil)
			g.Assert(err == nil).IsFalse()
		})
	})
}

func TestOpenDB(t *testing.T) {
	g := goblin.Goblin(t)
	g.Describe("#OpenDB", func() {
		g.It("Should return nil error", func() {
			_, err := OpenDB()
			g.Assert(err == nil).IsTrue()
		})
	})
}

func TestGetDBConf(t *testing.T) {
	g := goblin.Goblin(t)
	g.Describe("#GetDBConf", func() {
		g.It("Should return non nil error for empty path", func() {
			_, err := GetDBConf("")
			g.Assert(err == nil).IsFalse()
		})

		g.It("Should return non nil error for wrong path", func() {
			_, err := GetDBConf("qwe/123/qwe.yaml")
			g.Assert(err == nil).IsFalse()
		})
	})
}

func TestTableName(t *testing.T) {
	g := goblin.Goblin(t)
	g.Describe("#TableName", func() {
		g.It("Config.TableName() should return config", func() {
			name := Config{}.TableName()
			g.Assert(name).Equal("config")
		})
		g.It("Data.TableName() should return datas", func() {
			name := Data{}.TableName()
			g.Assert(name).Equal("datas")
		})
		g.It("Type.TableName() should return types", func() {
			name := Type{}.TableName()
			g.Assert(name).Equal("types")
		})
	})
}
