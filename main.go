package main

import (
	"log"

	flag "github.com/spf13/pflag"
)

var (
	port       = flag.String("port", "3000", "port")
	dbconfPath = flag.String("dbconf", "dbconf.yaml", "path to dbconf.yaml")
)

func main() {
	flag.Parse()
	log.Fatal(NewServer("3000"))
}
