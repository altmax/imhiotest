FROM golang:1.9-alpine

RUN apk add --no-cache git

RUN go get bitbucket.org/altmax/imhiotest

RUN go get -tags nosqlite3 github.com/steinbacher/goose/cmd/goose

RUN go get -u github.com/kardianos/govendor

WORKDIR /go/src/bitbucket.org/altmax/imhiotest

RUN govendor sync

RUN go build

EXPOSE 3000

# RUN goose up

# CMD [ "./imhiotest" ]
ENTRYPOINT [ "goose", "up", "&&", "./imhiotest" ]

