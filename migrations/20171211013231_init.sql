-- +goose Up

CREATE TABLE types (
    id int NOT NULL AUTO_INCREMENT,
    name text,
    PRIMARY KEY(id)
);

CREATE TABLE datas (
    id int NOT NULL AUTO_INCREMENT,
    type_id int NOT NULL,
    name text,
    PRIMARY KEY(id)
);

CREATE TABLE config (
    id int NOT NULL AUTO_INCREMENT,
    data_id int NOT NULL,
    host text,
    port text,
    virtualhost text,
    user text,
    password text,
    `schema` text,
    `database` text,
    PRIMARY KEY(id)
);

-- +goose Down

DROP TABLE config;
DROP TABLE datas;
DROP TABLE types;