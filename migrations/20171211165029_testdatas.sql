-- +goose Up
-- SQL in section 'Up' is executed when this migration is applied

insert into types (name) values('Develop.mr_robot');
insert into types (name) values('Test.vpn');

insert into datas (name, type_id) values('Database.processing', 1);
insert into datas (name, type_id) values('Rabbit.Log', 2);

insert into config (data_id, host, port, virtualhost, user, password, `schema`, `database`) values(1, 'localhost', '5432', '', 'mr_robot', 'secret', 'public', 'devdb');
insert into config (data_id, host, port, virtualhost, user, password, `schema`, `database`) values(2, '10.0.5.42', '5671', '/', 'guest', 'guest', '', '');

-- +goose Down
-- SQL section 'Down' is executed when this migration is rolled back

delete from datas;
delete from types;
delete from config;