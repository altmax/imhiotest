package main

import (
	"errors"
	"io/ioutil"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gopkg.in/yaml.v2"
)

type Config struct {
	DataID      int    `xml:"data_id" json:"-" db:"data_id"`
	Host        string `xml:"host" json:"host,omitempty" db:"host"`
	Port        string `xml:"port" json:"port,omitempty" db:"port"`
	Database    string `xml:"database" json:"database,omitempty" db:"database"`
	User        string `xml:"user" json:"user,omitempty" db:"user"`
	Password    string `xml:"password" json:"password,omitempty" db:"password"`
	Schema      string `xml:"schema" json:"schema,omitempty" db:"schema"`
	Virtualhost string `xml:"virtualhost" json:"virtualhost,omitempty" db:"virtualhost"`
}

func (Config) TableName() string {
	return "config"
}

type Data struct {
	ID     int
	Name   string
	TypeID int
}

func (Data) TableName() string {
	return "datas"
}

type Type struct {
	ID   int
	Name string
}

func (Type) TableName() string {
	return "types"
}

func GetConfigByQuery(q *Query) (*Config, error) {
	if q == nil {
		return nil, errors.New("Bad query")
	}

	db, err := OpenDB()
	if err != nil {
		return nil, err
	}
	defer db.Close()

	t := Type{}
	if err := db.Where(&Type{Name: q.Type}).First(&t).Error; err != nil {
		return nil, err
	}
	if t.Name != q.Type {
		return nil, errors.New("record not found")
	}

	d := Data{}
	if err := db.Where(&Data{Name: q.Data, TypeID: t.ID}).First(&d).Error; err != nil {
		return nil, err
	}

	c := Config{}
	if err := db.Where(&Config{DataID: d.ID}).First(&c).Error; err != nil {
		return nil, err
	}
	return &c, nil
}

func OpenDB() (*gorm.DB, error) {
	c, err := GetDBConf(*dbconfPath)
	if err != nil {
		return nil, err
	}

	return gorm.Open(c.Driver, c.DSN)
}

type DBConf struct {
	Driver string
	DSN    string
}

func GetDBConf(dbconfPath string) (*DBConf, error) {
	dbconf, err := ioutil.ReadFile(dbconfPath)
	if err != nil {
		return nil, err
	}

	c := DBConf{}
	err = yaml.Unmarshal(dbconf, &c)
	if err != nil {
		return nil, err
	}
	return &c, nil
}
