package main

type Query struct {
	Type string `xml:"type" json:"Type,omitempty" db:"type"`
	Data string `xml:"data" json:"Data,omitempty" db:"data"`
}
