package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
)

func NewServer(port string) error {
	h := MainHandler()
	_, err := strconv.ParseInt(port, 10, 64)
	if err != nil {
		return err
	}
	log.Println("Server listening on port: " + port)
	return http.ListenAndServe(":"+port, h)
}

func MainHandler() *http.ServeMux {
	m := http.NewServeMux()
	m.HandleFunc("/", MainHandleFunc)
	return m
}

func MainHandleFunc(w http.ResponseWriter, r *http.Request) {
	query := r.FormValue("query")
	res, code := Logic([]byte(query))
	w.WriteHeader(code)
	w.Write(res)
}

//Logic work with query and return result or error
func Logic(query []byte) ([]byte, int) {
	q, err := ParseQuery(query)
	if err != nil {
		return []byte("Bad request"), 400
	}
	res, err := GetConfigByQuery(q)
	if err != nil {
		if err.Error() == "record not found" {
			return []byte("Not found"), 404
		}
		return []byte("Internal error: " + err.Error()), 500
	}
	resJSON, err := json.Marshal(res)
	if err != nil {
		return []byte("Internal error"), 500
	}
	return resJSON, 200
}

//ParseQuery binding query byte slice to query struct
func ParseQuery(query []byte) (*Query, error) {
	var q Query
	err := json.Unmarshal(query, &q)
	if err != nil {
		return nil, err
	}
	return &q, nil
}
