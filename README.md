## Install

```
go get bitbucket.org/altmax/imhiotest
```

## Usage

- Подтянуть зависимости из vendor/vendor.json
- Исправить dbconf.yaml
- Поднять миграции

Приложение принимает два флага:

- --port - номер порта, который будет слушать сервер
- --dbconfPath - путь к файлу dbconf.yaml

### Vendoring

Govendor - https://github.com/kardianos/govendor

### Migrations

Goose - https://github.com/pressly/goose

### ORM

Gorm - https://github.com/jinzhu/gorm

### Testing

Goblin - https://github.com/franela/goblin