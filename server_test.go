package main

import (
	"testing"

	"github.com/franela/goblin"
)

func TestParseQuery(t *testing.T) {
	g := goblin.Goblin(t)
	g.Describe("#ParseQuery", func() {
		g.It("Should return nil error and parse query", func() {
			expected := Query{
				Type: "Develop.mr_robot",
				Data: "Database.processing",
			}
			input := []byte(`{"Type": "Develop.mr_robot","Data": "Database.processing"}`)
			q, err := ParseQuery(input)
			g.Assert(err == nil).IsTrue()
			g.Assert(*q).Equal(expected)
		})

		g.It("Should return non nil error", func() {
			input := []byte(`{"Type": "Develop.mr_robot""Data": "Database.processing"}`)
			_, err := ParseQuery(input)
			g.Assert(err == nil).IsFalse()
			// g.Assert(*q).Equal(expected)
		})
	})
}

func TestLogic(t *testing.T) {
	g := goblin.Goblin(t)
	g.Describe("#Logic", func() {
		g.It("Should return code 400 and Bad request", func() {
			input := []byte(`{"Type": "Develop.mr_robot""Data": "Database.processing"}`)
			buf, code := Logic(input)
			g.Assert(code).Equal(400)
			g.Assert(buf).Equal([]byte("Bad request"))
		})

		g.It("Should return code 200", func() {
			input := []byte(`{"Type": "Develop.mr_robot", "Data": "Database.processing"}`)
			_, code := Logic(input)
			g.Assert(code).Equal(200)
			// g.Assert(buf).Equal([]byte("Bad request"))
		})

		g.It("Should return code 404 and not found", func() {
			input := []byte(`{"Type": "Develop.vpn", "Data": "Database.processing"}`)
			buf, code := Logic(input)
			g.Assert(code).Equal(404)
			g.Assert(string(buf)).Equal("Not found")
		})
	})
}

func TestNewServer(t *testing.T) {
	g := goblin.Goblin(t)
	g.Describe("#NewServer", func() {
		g.It("Should return non nil error for port:'qwe'", func() {
			err := NewServer("qwe")
			g.Assert(err == nil).IsFalse()
		})
	})
}
